import pygame
import random
import time
import numpy as np
from shapes import J, L, O


WIN_WIDTH = 400
WIN_HEIGHT = 600
PLAY_WIDTH = 300
PLAY_HEIGHT = 500
BLOCK_SIZE = 20
TOP_LEFT_X = 50
TOP_LEFT_Y = 50
shapes = [O, J, L]
colors = ((255, 0, 0), (0, 255, 0), (0, 0, 255))


class Brick(object):
    def __init__(self, x, y, shape, rot=0, color=(255, 0, 0)):
        self.shape = shape
        self.positions = np.array(self.create_brick(x, y))
        self.rotation = rot
        self.def_color = colors[shapes.index(shape)]
        self.color = self.def_color

    def move_left(self):
        self.positions[0] -= 1
    def move_right(self):
        self.positions[0] += 1
    def move_up(self):
        self.positions[1] -= 1
    def move_down(self):
        self.positions[1] += 1
    def rotate(self, rot):
        self.rotation += rot
        x = self.positions[0][0].copy()
        y = self.positions[1][0].copy()
        for i in range(len(self.positions[0])):

            self.positions[0][i] = x + self.shape[self.rotation % len(self.shape)][i][0]
            self.positions[1][i] = y + self.shape[self.rotation % len(self.shape)][i][1]
    def make_move(self, move):
        if move == 0:
            self.move_right()
        elif move == 1:
            self.move_up()
        elif move == 2:
            self.move_left()
        elif move == 3:
            self.move_down()

    def current_color(self):
        self.color = (255, 255, 255)
    def default_color(self):
        self.color = self.def_color
    def create_brick(self, x, y):  #returns position of each piece of block
        x_positions = []
        y_positions = []
        for i, j in self.shape[0]:
            x_positions.append(x + i)
            y_positions.append(y + j)
        return (np.array(x_positions), np.array(y_positions))
    def valid_space(self, locked_position, move, previous_pos=0):
        accepted_pos = [[(j, i) for j in range(int(PLAY_WIDTH / BLOCK_SIZE)) if (j, i) not in locked_position] for i in
                        range(int(PLAY_HEIGHT / BLOCK_SIZE))]
        accepted_pos = [j for sub in accepted_pos for j in sub]

        if move:
            for i in range(len(previous_pos[0])):
                accepted_pos.append((previous_pos[0][i], previous_pos[1][i]))

        for i in range(len(self.positions[0])):
            if (self.positions[0][i], self.positions[1][i]) not in accepted_pos:
                return False
        return True
# CREATING FUNCTIONS
def create_grid(locked_pos={}):
    grid = [[(0, 0, 0) for _ in range(int(PLAY_WIDTH/BLOCK_SIZE)+1)] for _ in range(int(PLAY_HEIGHT/BLOCK_SIZE)+1)]

    for i in range(len(grid)):
        for j in range (len(grid[i])):
            if (j, i) in locked_pos:
                color = locked_pos[(j, i)].def_color
                grid[i][j] = color
    return grid
def draw_grid(surface, grid):
    for i in range(len(grid)):
        pygame.draw.line(surface, (120, 120, 120), (TOP_LEFT_X, TOP_LEFT_Y + i*BLOCK_SIZE), (TOP_LEFT_X + PLAY_WIDTH, TOP_LEFT_Y + i*BLOCK_SIZE))
        for j in range(len(grid[i])):
            pygame.draw.line(surface, (120, 120, 120), (TOP_LEFT_X + j*BLOCK_SIZE, TOP_LEFT_Y ), (TOP_LEFT_X + j*BLOCK_SIZE, TOP_LEFT_Y + PLAY_HEIGHT))
def draw_window(surface, grid):
    surface.fill((0, 0, 0))
    pygame.font.init()
    font = pygame.font.SysFont('comicsans', 60)
    label = font.render("Lab 7", 1, (255, 255, 255))
    surface.blit(label, (TOP_LEFT_X + PLAY_WIDTH / 2 - (label.get_width() / 2), 10))
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            pygame.draw.rect(surface, grid[i][j], (TOP_LEFT_X + j * BLOCK_SIZE, TOP_LEFT_Y + i * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE), 0)
    draw_grid(surface, grid)
    pygame.display.update()
def add_bricks(count, grid, locked_positions):
    bricks = []
    for _ in range(count):
        valid = False
        while not valid:
            brick = Brick(random.randint(0, int(PLAY_WIDTH/BLOCK_SIZE)), random.randint(0, int(PLAY_HEIGHT/BLOCK_SIZE)), random.choice(shapes))
            brick.rotate(random.randint(0, 4))
            valid = brick.valid_space(locked_positions, False)
        for i in range(len(brick.positions[0])):
            grid[brick.positions[1][i]][brick.positions[0][i]] = brick.color
            p = (brick.positions[0][i], brick.positions[1][i])
            locked_positions[p] = brick.color
        bricks.append(brick)
    return bricks
def update_locked_pos(bricks, grid):
    locked_positions = {}
    for brick in bricks:
        for i in range(len(brick.positions[0])):
            grid[brick.positions[1][i]][brick.positions[0][i]] = brick.color
            p = (brick.positions[0][i], brick.positions[1][i])
            locked_positions[p] = brick
    return locked_positions
def update_grid_color(grid, brick):
    for i in range(len(brick.positions[0])):
        grid[brick.positions[1][i]][brick.positions[0][i]] = brick.color
#GAMEPLAY FUNCTION
def play(bricks, grid, BRICKS_COUNT):
    brick_num = 0
    current_brick = bricks[brick_num]
    current_brick.current_color()
    run = True
    while run:
        locked_positions = update_locked_pos(bricks, grid)
        grid = create_grid(locked_positions)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    previous_pos = current_brick.positions.copy()
                    current_brick.move_left()
                    if not (current_brick.valid_space(locked_positions, True, previous_pos)):
                        current_brick.move_right()
                if event.key == pygame.K_RIGHT:
                    previous_pos = current_brick.positions.copy()
                    current_brick.move_right()
                    if not (current_brick.valid_space(locked_positions, True, previous_pos)):
                        current_brick.move_left()
                if event.key == pygame.K_DOWN:
                    previous_pos = current_brick.positions.copy()
                    current_brick.move_down()
                    if not (current_brick.valid_space(locked_positions, True, previous_pos)):
                        current_brick.move_up()
                if event.key == pygame.K_UP:
                    previous_pos = current_brick.positions.copy()
                    current_brick.move_up()
                    if not (current_brick.valid_space(locked_positions, True, previous_pos)):
                        current_brick.move_down()
                if event.key == pygame.K_SPACE:
                    brick_num += 1
                    current_brick.default_color()
                    current_brick = bricks[brick_num % BRICKS_COUNT]
                    current_brick.current_color()
                if event.key == pygame.K_TAB:
                    previous_pos = current_brick.positions.copy()
                    current_brick.rotate(1)
                    if not (current_brick.valid_space(locked_positions, True, previous_pos)):
                        current_brick.rotate(-1)

        update_grid_color(grid, current_brick)
        draw_window(win, grid)
# ALGORITHM FUNCTIONS
def group_move(group_of_bricks, grid, bricks, const_move):
    change_direction = 0
    for _ in range(7):
        any_new_move = True
        for brick in group_of_bricks:
            for _ in range(10):
                locked_positions = update_locked_pos(bricks, grid)

                previous_pos = brick.positions.copy()
                if change_direction == 0:
                    brick.make_move(const_move)
                    if not (brick.valid_space(locked_positions, True, previous_pos)):
                        brick.make_move((2 + const_move) % 4)
                    else:
                        any_new_move = False
                else:
                    brick.make_move((const_move + 1) % 4)
                    if not (brick.valid_space(locked_positions, True, previous_pos)):
                        brick.make_move((3 + const_move) % 4)
                    else:
                        any_new_move = False

                change_direction = (change_direction + 1) % 2

                grid = create_grid(locked_positions)

                update_grid_color(grid, brick)
                draw_window(win, grid)
        if any_new_move == True:
            return
def group_color(color, bricks):
    group_bricks = []
    for brick in bricks:
        if brick.def_color == color:
            group_bricks.append(brick)
    return group_bricks
def prepare_map(bricks, grid):

    green_bricks = group_color((0, 255, 0), bricks)
    blue_bricks = group_color((0, 0, 255), bricks)
    red_bricks = group_color((255, 0, 0), bricks)

    group_move(red_bricks, grid, bricks, 0)
    group_move(blue_bricks, grid, bricks, 2)
    group_move(green_bricks, grid, bricks, 1)
    group_move(red_bricks, grid, bricks, 3)
    group_move(green_bricks, grid, bricks, 0)
    group_move(blue_bricks, grid, bricks, 1)
    group_move(green_bricks, grid, bricks, 0)
    group_move(blue_bricks, grid, bricks, 1)
    group_move(red_bricks, grid, bricks, 2)
    group_move(green_bricks, grid, bricks, 3)
    group_move(blue_bricks, grid, bricks, 0)
    group_move(red_bricks, grid, bricks, 1)
    group_move(green_bricks, grid, bricks, 2)
    group_move(blue_bricks, grid, bricks, 3)
    group_move(green_bricks, grid, bricks, 0)
    group_move(blue_bricks, grid, bricks, 3)
    group_move(red_bricks, grid, bricks, 1)
    group_move(green_bricks, grid, bricks, 2)
    group_move(blue_bricks, grid, bricks, 3)
    group_move(red_bricks, grid, bricks, 1)

    return green_bricks, blue_bricks, red_bricks
def brick_to_center(central_x, central_y,brick, grid, bricks, start_with_y, change_direction=0):

    locked_positions = update_locked_pos(bricks, grid)
    previous_pos = brick.positions.copy()
    if brick.positions[0][0] > central_x and (not start_with_y or change_direction < 2):
        brick.move_left()
        if not (brick.valid_space(locked_positions, True, previous_pos)):
            brick.move_right()
    elif brick.positions[0][0] < central_x and (not start_with_y or change_direction < 2):
        brick.move_right()
        if not (brick.valid_space(locked_positions, True, previous_pos)):
            brick.move_left()
    elif brick.positions[1][0] < central_y:
        brick.move_down()
        if not (brick.valid_space(locked_positions, True, previous_pos)):
            brick.move_up()
    elif brick.positions[1][0] > central_y:
        brick.move_up()
        if not (brick.valid_space(locked_positions, True, previous_pos)):
            brick.move_down()
    if previous_pos[0][0] == brick.positions[0][0] and previous_pos[1][0] == brick.positions[1][0]:
        return False, None,change_direction
    if central_y == brick.positions[1][0]:
        start_with_y = False
    change_direction +=1
    grid = create_grid(locked_positions)
    update_grid_color(grid, brick)
    draw_window(win, grid)
    return True, start_with_y,change_direction
def rotate_and_refresh(brick, bricks, grid, rot):
    while (brick.rotation % len(brick.shape)) != rot:
        brick.rotate(1)
    locked_positions = update_locked_pos(bricks, grid)
    grid = create_grid(locked_positions)
    update_grid_color(grid, brick)
    draw_window(win, grid)
def make_move_to_crorner(brick, grid, bricks,up):
    locked_positions = update_locked_pos(bricks, grid)
    previous_pos = brick.positions.copy()
    if up:
        brick.move_up()
        if not (brick.valid_space(locked_positions, True, previous_pos)):
            brick.move_down()
            up = False
    else:
        brick.move_right()
        if not (brick.valid_space(locked_positions, True, previous_pos)):
            brick.move_left()

    grid = create_grid(locked_positions)
    update_grid_color(grid, brick)
    draw_window(win, grid)
    return up
def place_in_corner(first_brick, second_brick, grid, bricks):
    up = True
    for _ in range(20):
        up = make_move_to_crorner(first_brick,grid,bricks, up)
        if second_brick != None:
            make_move_to_crorner(second_brick, grid, bricks, up)
def prepare_block(blocks, grid, bricks, central_x, central_y, x_correction, y_correction, first_rot, second_rot,last_brick=False, start_with_y=False):
    first_brick = blocks[0]
    first_on_position = False
    second_brick = blocks[0]
    second_on_position = False
    while not second_on_position:
        for brick in blocks:
            change_direction = 0
            y_first = start_with_y
            next_move = True
            if not first_on_position:
                while not first_on_position and next_move:
                    next_move, y_first, change_direction = brick_to_center(central_x, central_y, brick, grid, bricks, y_first, change_direction)
                    if brick.positions[0][0] == central_x and brick.positions[1][0] == central_y:
                        first_on_position = True
                        rotate_and_refresh(brick, bricks, grid, first_rot)
                        first_brick = brick
                        blocks.remove(brick)
                        if last_brick:
                            return first_brick, None, None
            elif not second_on_position:
                while not second_on_position and next_move:
                    next_move, y_first, change_direction = brick_to_center(central_x + x_correction, central_y + y_correction, brick, grid, bricks, y_first, change_direction)
                    if np.absolute(brick.positions[0][0] + brick.positions[1][0] - central_x - x_correction - central_y - y_correction) == 3:
                        rotate_and_refresh(brick, bricks, grid, second_rot)
                    if brick.positions[0][0] == (central_x + x_correction) and brick.positions[1][0] == (central_y + y_correction):
                        second_on_position = True
                        second_brick = brick
                        blocks.remove(brick)
    return first_brick, second_brick, blocks
def solve(green_blocks, blue_blocks, red_blocks, grid, bricks):
    central_x = int((PLAY_WIDTH/BLOCK_SIZE)/2)
    #central_y = int((PLAY_HEIGHT/BLOCK_SIZE)/2)
    central_y = central_x
    green_length = len(green_blocks)
    for _ in range(int(green_length/2)):
            first_brick, second_brick, green_blocks = prepare_block(green_blocks,grid,bricks,central_x,central_y,1,1,0,2)
            place_in_corner(first_brick,second_brick, grid, bricks)
    blue_length = len(blue_blocks)
    for _ in range(int(blue_length/2)):
            first_brick, second_brick, blue_blocks = prepare_block(blue_blocks,grid,bricks,central_x+1,central_y,-1,1,0,2)
            place_in_corner(first_brick,second_brick, grid, bricks)
    red_length = len(red_blocks)
    for _ in range(int(red_length / 2)):
        first_brick, second_brick, red_blocks = prepare_block(red_blocks, grid, bricks, central_x+2 , central_y + 2,-2,0,0,0,False, True)
        place_in_corner(first_brick, second_brick, grid, bricks)

    if len(red_blocks) > 0:
        first_brick, second_brick, _ = prepare_block(red_blocks, grid, bricks, central_x+2 , central_y + 2,-2,0,0,0,True, True)
        place_in_corner(first_brick,second_brick, grid, bricks)
    if len(green_blocks) > 0:
        first_brick, second_brick, _ = prepare_block(green_blocks,grid,bricks,central_x,central_y,1,1,0,2,True)
        place_in_corner(first_brick,second_brick, grid, bricks)
    if len(blue_blocks) > 0:
        first_brick, second_brick, _ = prepare_block(blue_blocks, grid, bricks, central_x + 1, central_y, -1, 1, 0, 2, True)
        place_in_corner(first_brick, second_brick, grid, bricks)


def main():
    locked_positions = {}
    BRICKS_COUNT = 15

    print("\n\n Welcome to Lab6-Game"
          "\n After the algorithm finishes, wait 5 seconds to start the game")
    grid = create_grid(locked_positions)
    bricks = add_bricks(BRICKS_COUNT, grid, locked_positions)

    green_blocks, blue_blocks, red_blocks = prepare_map(bricks, grid)
    solve(green_blocks, blue_blocks, red_blocks,  grid, bricks)
    time.sleep(5)

    locked_positions = {}
    grid = create_grid(locked_positions)
    bricks = add_bricks(BRICKS_COUNT, grid, locked_positions)
    print("\n ===========Keyboard============="
          "\n\n TAB - rotate block"
          "\n SPACE - change block"
          "\n ARROW UP - move up"
          "\n ARROW DONW - move down"
          "\n ARROW RIGHT - move right"
          "\n ARROW LEFT - move left")
    play(bricks, grid, BRICKS_COUNT)

pygame.font.init()
win = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
pygame.display.set_caption("Broda_171652")

main()




